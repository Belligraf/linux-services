import socket


def main():
    ip = input('ip: ')
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((ip, 1303))

    data = sock.recv(1024)
    sock.close()

    print(data.decode('utf-8'))


if __name__ == '__main__':
    main()
