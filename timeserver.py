import socket

from datetime import datetime


def main():
    sock = socket.socket()
    sock.bind(('', 1303))
    sock.listen(1)

    while True:
        conn, addr = sock.accept()
        today = str(datetime.now().strftime('%d.%m.%Y %H:%M'))
        # print(*addr, today)
        conn.send(today.encode('utf-8'))
        conn.close()


if __name__ == '__main__':
    main()
