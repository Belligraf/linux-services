creat by Бронников Родион 11-007

<h2> Как запустить </h2>

Скачиваем проект командой

    git clone https://gitlab.com/Belligraf/linux-services.git

Затем переходим в директорию и запускаем сервер командой

    python3 timeserver.py

После чего в другом терминале запускаем клиент и вводим ip 127.0.0.1

    python3 timeclient.py
